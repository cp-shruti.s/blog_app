-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 15, 2020 at 09:46 AM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `AdminUser_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `Admins`
--

CREATE TABLE `Admins` (
  `admin_id` int(6) UNSIGNED NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(25) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Admins`
--

INSERT INTO `Admins` (`admin_id`, `firstname`, `lastname`, `email`, `username`, `password`) VALUES
(2, 'shruti', 'sonani', 'shruti@gmail.com', 'shrutisonani', 'eab6930b3c87b22874b40a0e52fe1ca3');

-- --------------------------------------------------------

--
-- Table structure for table `is_like`
--

CREATE TABLE `is_like` (
  `post_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `is_like`
--

INSERT INTO `is_like` (`post_id`, `user_id`) VALUES
(2, 1),
(2, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Posts`
--

CREATE TABLE `Posts` (
  `post_id` int(6) UNSIGNED NOT NULL,
  `title` varchar(25) NOT NULL,
  `description` varchar(100) NOT NULL,
  `dates` date NOT NULL,
  `author` varchar(20) NOT NULL,
  `category` varchar(25) NOT NULL,
  `image` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Posts`
--

INSERT INTO `Posts` (`post_id`, `title`, `description`, `dates`, `author`, `category`, `image`) VALUES
(2, 'hello', 'hey book', '1998-04-02', 'hii', 'history', 'book.jpg'),
(3, 'bbb', 'bbb', '2017-10-10', 'bbbb', 'bbbb', 'book3.jpg'),
(4, 'fvd', 'cvvds', '2017-10-10', 'vcdf  ', 'dfvszdf', 'Haunted.jpg'),
(5, 'yybfgb ', 'fgrgtth', '2020-09-12', 'fgbhnytjmik', 'qq32423vfd', 'Horror.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `user_id` int(6) UNSIGNED NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`user_id`, `firstname`, `lastname`, `address`, `username`, `password`) VALUES
(1, 'bhumi', 'patel', 'katargam', 'bhumipatel', '486516d83c98fc0f40d2e3d8b168ec81'),
(3, 'mansi', 'soni', 'katargam', 'mansisoni', '8e183f28f7ac8aaebf5650f728f79a37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Admins`
--
ALTER TABLE `Admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `Posts`
--
ALTER TABLE `Posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Admins`
--
ALTER TABLE `Admins`
  MODIFY `admin_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Posts`
--
ALTER TABLE `Posts`
  MODIFY `post_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `user_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
