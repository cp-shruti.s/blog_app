<?php

if (isset($_POST['submit'])) {
    include ("config.php");

    $targetDir = "Image/";
    $fileName = basename($_FILES["image"]["name"]);
    $targetFilePath = $targetDir . $fileName;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

    $allowTypes = array('jpg','png','jpeg','gif','pdf');
    if (!file_exists($targetFilePath)) {
        if(in_array($fileType, $allowTypes)){
            // Upload file to server
            if(move_uploaded_file($_FILES["image"]["tmp_name"], $targetFilePath)){
                // Insert image file name into database
                $new = "insert into Posts (title,description,dates,author,category,image) values ('$_POST[title]','$_POST[description]','$_POST[date]','$_POST[author]','$_POST[category]','".$fileName."')";
                if(mysqli_query($con, $new)){
                    echo "success poat <br>";
                    header("location:admin_Posts.php");
                } else {
                    echo "failure";
                }
            }else{
                $statusMsg = "Sorry, there was an error uploading your file." ;
            }
        }else{
            $statusMsg = "Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload." ;
        }
    }else{
        $statusMsg = "The file <b>".$fileName. "</b> is already exist." ;
    }
}
?>

<!DOCTYPE html>
<html lang="em">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="center">
    <form action="Admin_HomePage.php" method="post" enctype="multipart/form-data">
        <div class="container">
            <h1>Register</h1>
            <p>Please fill in this form </p>
            <hr>

            <label for="title"><b>Posts Title</b></label>
            <input type="text" placeholder="Enter Posts Title" name="title"  required>

            <label for="description"><b>Posts Description</b></label>
            <input type="text" placeholder="Enter Posts Description" name="description"  required>

            <label for="date"><b>Posts Created Date</b></label>
            <input type="text" placeholder="Enter Published date" name="date"  required>

            <label for="author"><b>Author Name</b></label>
            <input type="text" placeholder="Enter Author name" name="author" required>

            <label for="category"><b>Posts Category</b></label>
            <input type="text" placeholder="Enter Category name" name="category"  required>

            <label for="image"><b>Image</b></label>
            <input type="file"  name="image" id="psw" accept="Image/" required>

            <hr>
            <button type="submit" class="registerbtn" name="submit" >Submit</button>
        </div>
    </form>
</div>
</body>
</html>


